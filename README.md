# Introduction
This is a repository which contains the Raw Dump of Aadhaar authentication records from Andhra. 

It is scrapped out from [the benefit portal](http://tspost.aponline.gov.in/PostalWebPortal/UserInterface/ReportsMain_TG.aspx).

# Analysis
Analysis is only possible via a Big Data program and that will be added 
soon enough. 

# Raw Data
Raw data is available in the "raw-data" folder. 